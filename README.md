# Cyclink - Aplicație de gestionare a evenimentelor de ciclism

Link github - https://github.com/Balentiu/cyclink.git

Link gitlab - https://gitlab.upt.ro/alexandru.pop2/cyclink.git

## Instalare

### Clonarea repository-ului

Se rulează comanda `git clone "https://github.com/Balentiu/cyclink.git"` în terminal.

### Configurare

1. Navighează spre repository-ul clonat: `cd cyclink`
2. Instalează dependințele proiectului:

`npm install react-router-dom`

`npm install firebase`

`npm install @react-ggogle-maps`

### Build&run

Pentru build&run, rulează comanda:

`npm start`
